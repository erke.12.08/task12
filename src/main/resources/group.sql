create table Student (
	studentId serial not null
constraint auto_pk unique ,
	studentName VARCHAR(50),
	phone VARCHAR(50),
	groupId bigint
);
insert into Student (studentId, studentName, phone, groupId) values (1, 'Agnès', '560-19-7477', 2);
insert into Student (studentId, studentName, phone, groupId) values (2, 'Lài', '598-39-1620', 1);
insert into Student (studentId, studentName, phone, groupId) values (3, 'Mårten', '855-95-1558', 1);



create table Group (
	grouptId INT,
	groupName TEXT
);
insert into Group (grouptId, groupName) values (1, 'Cs');
insert into Group (grouptId, groupName) values (2, 'IT');
insert into Group (grouptId, groupName) values (3, 'TC');