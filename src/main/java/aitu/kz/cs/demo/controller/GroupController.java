package aitu.kz.cs.demo.controller;


import aitu.kz.cs.demo.model.Group;
import aitu.kz.cs.demo.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GroupController {
    private final GroupRepository groupRepository;

    public GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }
    @GetMapping(path="/groups")
    public ResponseEntity<?> getGroup() {
        return ResponseEntity.ok(groupRepository.findAll());
    }







}
