package aitu.kz.cs.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Group")
public class Group {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int groupId;
    String groupName;
}
