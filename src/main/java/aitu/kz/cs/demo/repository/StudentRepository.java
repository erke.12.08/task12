package aitu.kz.cs.demo.repository;


import aitu.kz.cs.demo.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  StudentRepository extends CrudRepository <Student, Integer> {
    List<Student> findAllByGroupId(long groupId);

    List<Student> findAll();

}
